import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MathTopicModule } from './components/math-topics/mathtopics.module';
import { DatabaseModule } from './database/database.module';
import { MathsubtopicModule } from './components/mathsubtopic/mathsubtopic.module';
import { UserModule } from './components/user/user.module';
import { AuthModule } from './components/auth/auth.module';
import { MathtestModule } from './components/mathtest/mathtest.module';
import { MathtesttaskModule } from './components/mathtesttask/mathtesttask.module';

@Module({
  imports: [
    MathTopicModule,
    MathsubtopicModule,
    MathtestModule,
    DatabaseModule,
    UserModule,
    AuthModule,
    MathtesttaskModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
