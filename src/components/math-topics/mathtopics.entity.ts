import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { MathSubtopic } from '../mathsubtopic/mathsubtopic.entity';

@Entity()
export class MathTopic {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 255 })
    name: string;

    @Column ()
    class: number;

    @OneToMany(type => MathSubtopic, topic => topic.mathTopic, { cascade: ['insert', 'update'] })
    subtopics: MathSubtopic[];

}