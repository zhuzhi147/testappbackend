import { Module } from '@nestjs/common';
import { MathTopicController } from './mathtopic.controller';
import { MathTopicsService } from './mathtopics.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MathTopic } from './mathtopics.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([MathTopic]),
  ],
  controllers: [MathTopicController],
  providers: [MathTopicsService]
})
export class MathTopicModule {}
