import { Injectable, HttpException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { MathTopic } from './mathtopics.entity';
import { Repository, getRepository } from "typeorm";

@Injectable()
export class MathTopicsService {

    constructor(@InjectRepository(MathTopic) private mathTopicRepository: Repository<MathTopic>) { }

    async addMathTopic(mathTopic: MathTopic): Promise<MathTopic> {
        return await this.mathTopicRepository.save(mathTopic);
    }

    async getMathTopics(): Promise<MathTopic[]> {
        return await this.mathTopicRepository.find();
    }

    async getMathTopicsByClass(schoolClass: number): Promise<MathTopic[]> {
        return await this.mathTopicRepository.find({
            class: schoolClass
        });
    }
}