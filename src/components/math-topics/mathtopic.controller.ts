import { Controller, Post, Body, Get, Query } from "@nestjs/common";
import { MathTopicsService } from "./mathtopics.service";
import { create } from "domain";
import { MathTopic } from "./mathtopics.entity";

@Controller('mathtopic')
export class MathTopicController {

    constructor(private mathtopicService: MathTopicsService) {}

    @Post('add')
    async create(@Body() mathTopic: MathTopic) {
        return await this.mathtopicService.addMathTopic(mathTopic);
    }

    @Get('all')
    async get() {
        return await this.mathtopicService.getMathTopics();
    }

    @Get('byClass')
    async getByClass(@Query('class') schoolclass: number){
        return await this.mathtopicService.getMathTopicsByClass(schoolclass); 
    }
}