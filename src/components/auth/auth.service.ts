import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { UserService } from '../user/user.service';
import * as jwt from 'jsonwebtoken';
import { LoginUserDTO } from '../user/dto/login.user';
import { JwtPayload } from './interface/jwt-payload.interface';

@Injectable()
export class AuthService {
    constructor(@Inject(forwardRef(() => UserService)) private readonly userService: UserService) {}

    async createToken(credentials: LoginUserDTO) {
        const user = await this.userService.login(credentials);
        const expiresIn = 60 * 60;
        const accessToken = jwt.sign({id: user.id, type: user.type}, 'rusk@sal@ta', {
            expiresIn,
        });
        return {
            expiresIn,
            accessToken,
            type: user.type,
            id: user.id,
        };
    }

    async verifyToken(token: string) {
        return new Promise(resolve => {
            token = token.replace(/^Bearer\s/, '');
            jwt.verify(token, 'rusk@sal@ta', () => resolve({success: true}));
        });
    }

    async validateUser(payload: JwtPayload): Promise<any> {
        return await this.userService.findOneById(payload.id);
    }
}
