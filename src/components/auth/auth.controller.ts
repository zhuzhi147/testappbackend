import { Controller, Post, Body, Get, UseGuards, Headers, UsePipes, ValidationPipe } from '@nestjs/common';
import { LoginUserDTO } from '../user/dto/login.user';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {

    constructor(private readonly authService: AuthService) {}
    
    @Get('verify')
    public async verify(@Headers('authorization') token: string) {
        return this.authService.verifyToken(token);
    }

    @Post('login')
    @UsePipes(
        new ValidationPipe({
          transform: true,
          whitelist: true,
          forbidNonWhitelisted: true,
        }),
      )
    public async login(@Body() credentials: LoginUserDTO) {
        return await this.authService.createToken(credentials);
    }
}
