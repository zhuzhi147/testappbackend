import { BaseEntity, Column, PrimaryGeneratedColumn, BeforeInsert, Entity, OneToOne, JoinColumn } from "typeorm";
import { IsEmail, Validate } from 'class-validator';
import { passwordHash } from "src/helper/password/password.hash";
import { Exclude } from 'class-transformer';
import { IsUsernameValid } from "./username.validator";
import { Teacher } from "./teacher/teacher.entity";

@Entity()
export class UserEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 255, nullable: false })
    name: string;

    @Column({ length: 255, unique: true, nullable: false })
    username: string;

    @Column({ length: 255, nullable: false })
    type: string;

    @Column({ length: 255, unique: true, nullable: false })
    @IsEmail()
    email: string;

    @Column({ length: 255, nullable: false })
    @Exclude()
    password: string;

    @OneToOne(type => Teacher, teacher => teacher.user, { cascade: true,  nullable: true, onUpdate: 'CASCADE', onDelete: 'CASCADE' })
    @JoinColumn()
    teacher: Teacher;

    @BeforeInsert()
    hashPassword() {
        this.password = passwordHash(this.password);
    }
}