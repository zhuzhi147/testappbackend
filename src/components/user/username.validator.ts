import {ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments} from "class-validator";
import { UserService } from "./user.service";
import { Inject, forwardRef } from "@nestjs/common";
 
@ValidatorConstraint({ name: "customText", async: false })
export class IsUsernameValid implements ValidatorConstraintInterface {
    constructor(@Inject(forwardRef(() => UserService)) private readonly userService: UserService) {}

    validate(text: string, args: ValidationArguments) {
        const username = this.userService.findOne({
            username: text
        });

        return !username;
    }
 
    defaultMessage(args: ValidationArguments) { // here you can provide default error message if validation failed
        return "Text ($value) is too short or too long!";
    }
 
}