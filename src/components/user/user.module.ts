import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './user.entity';
import { Teacher } from './teacher/teacher.entity';
import { TeacherService } from './teacher/teacher.service';
import { TeacherController } from './teacher/teacher.controller';

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity, Teacher])],
  controllers: [UserController, TeacherController],
  providers: [UserService, TeacherService],
  exports: [UserService]
})
export class UserModule {}
