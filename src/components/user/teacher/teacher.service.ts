import { Injectable, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Teacher } from './teacher.entity';
import { Repository, UpdateResult, DeepPartial } from 'typeorm';

@Injectable()
export class TeacherService {
    constructor(@InjectRepository(Teacher) private repository: Repository<Teacher>) {}

    public async updateTeacherStatus(teacher: DeepPartial<Teacher>): Promise<any> {
        const result = await this.repository.update(teacher.id, {
            approved: teacher.approved
        });

        if (!result) {
            throw new HttpException('teacher not found', 500);
        }

        return result;
    }
}
