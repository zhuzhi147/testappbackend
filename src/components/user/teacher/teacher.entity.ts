import { Entity, PrimaryGeneratedColumn, Column, OneToOne } from "typeorm";
import { UserEntity } from "../user.entity";

@Entity()
export class Teacher {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 255, nullable: false })
    name: string;

    @Column({ nullable: true, default: false })
    approved: boolean;

    @OneToOne(type => UserEntity, user => user.teacher)
    user: UserEntity;
}