import { Controller, Put, Body } from '@nestjs/common';
import { TeacherService } from './teacher.service';
import { Teacher } from './teacher.entity';
import { DeepPartial } from 'typeorm';

@Controller('teacher')
export class TeacherController {
    constructor(private teacherService: TeacherService) {}

    @Put()
    async updateTeacherStatus(@Body() teacher: DeepPartial<Teacher>) {
        return await this.teacherService.updateTeacherStatus(teacher);
    }
}
