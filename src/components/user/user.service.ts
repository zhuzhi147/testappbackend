import { Injectable, NotFoundException, UnprocessableEntityException, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, FindConditions, FindOneOptions, DeepPartial } from 'typeorm';
import { UserEntity } from './user.entity';
import { LoginUserDTO } from './dto/login.user';
import { passwordHash } from 'src/helper/password/password.hash';
import { validate } from 'class-validator';

@Injectable()
export class UserService {
    constructor(@InjectRepository(UserEntity) private readonly userRepository: Repository<UserEntity>) {}

    public async findOne(conditions?: FindConditions<UserEntity>, options?: FindOneOptions): Promise<UserEntity> {
        return this.userRepository.findOne(conditions, options);
    }

    public async findOneById(id: number): Promise<UserEntity> {
        return this.userRepository.findOne(id);
    }

    public async register(user: DeepPartial<UserEntity>): Promise<UserEntity> {
        const createUser = this.userRepository.create(user);
        await this.validate(createUser);

        return createUser.save();
    }

    public async findAll(): Promise<UserEntity[]> {
        const users = await UserEntity.createQueryBuilder('user')
        .leftJoinAndSelect('user.teacher', 'teacher')
        .where('teacher.approved = :approved', { approved: false})
        .getMany();

        users.map(u => delete u.password);

        return users;
    }
    
    public async login(credentials: LoginUserDTO) {
        const user = await this.userRepository.findOne({
            username: credentials.username,
            password: passwordHash(credentials.password),
        }, { relations: ['teacher'] });

        if (!user) {
            throw new NotFoundException('Не е намерен такъв потребител!');
        }

        if(user.teacher && !user.teacher.approved) {
            throw new HttpException('Не е одобрен все още този профил!', 500);
        }

        return user;
    }

    private async validate(entity: UserEntity) {
        const errors = await validate(entity);
        if (errors.length) {
            throw new UnprocessableEntityException(errors);
        }
    }
}
