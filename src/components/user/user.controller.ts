import { Controller, Post, Body, Get } from '@nestjs/common';
import { UserService } from './user.service';
import { DeepPartial } from 'typeorm';
import { UserEntity } from './user.entity';

@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService) {}

    @Post('signup')
    async register(@Body() user: DeepPartial<UserEntity>): Promise<UserEntity> {
        return this.userService.register(user);
    }

    @Get()
    async getAll(): Promise<UserEntity[]> {
        return await this.userService.findAll();
    }
}
