import { Test, TestingModule } from '@nestjs/testing';
import { MathtesttaskService } from './mathtesttask.service';

describe('MathtesttaskService', () => {
  let service: MathtesttaskService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MathtesttaskService],
    }).compile();

    service = module.get<MathtesttaskService>(MathtesttaskService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
