import { Controller, Get, Query } from '@nestjs/common';
import { MathtesttaskService } from './mathtesttask.service';

@Controller('mathtesttask')
export class MathtesttaskController {

    constructor(private mathTestTaskService: MathtesttaskService) {}

    @Get()
    async getTasks(@Query('mathTestId') mathTestId: number, @Query('take') take, @Query('skip') skip) {
        return await this.mathTestTaskService.getTasks(mathTestId, { take, skip });
    }

    @Get('answer')
    async correctAnswer(@Query('mathTaskId') mathTaskId: number, @Query('answer') answer: string) {
        return await this.mathTestTaskService.checkCorrectAnswer(mathTaskId, answer);
    }
}
