import { Injectable, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { MathTestTask } from './mathtesttask.entity';

@Injectable()
export class MathtesttaskService {
    constructor(@InjectRepository(MathTestTask) private repository: Repository<MathTestTask>) {}

    public async getTasks(mathTestId: number, query): Promise<[MathTestTask, number] | Response> {
        const take = 1;
        const skip = query.skip || 0

        const [result, total] = await this.repository.findAndCount({
            where: { mathTest: { id: mathTestId } },
            take,
            skip 
        });

        return [
            result[0], total 
        ]
    }

    public async checkCorrectAnswer(mathTaskId: number, answer: string): Promise<{ answer: boolean }> {
        const mathTask = await this.repository.findOne(mathTaskId);
        if (!mathTask) {
            throw new HttpException('Task not found', 500);
        }
        const mathAnswer = mathTask.correctAnswer === answer;

        return {
            answer: mathAnswer
        }
    }
}
