import { Test, TestingModule } from '@nestjs/testing';
import { MathtesttaskController } from './mathtesttask.controller';

describe('Mathtesttask Controller', () => {
  let controller: MathtesttaskController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MathtesttaskController],
    }).compile();

    controller = module.get<MathtesttaskController>(MathtesttaskController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
