import { PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, Entity } from "typeorm";
import { MathTest } from "../mathtest/mathtest.entity";

@Entity()
export class MathTestTask {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 255 })
    task: string;

    @Column({ length: 255 })
    image: string;

    @Column({ length: 255 })
    answers: string;

    @Column({ length: 255 })
    correctAnswer: string;

    @ManyToOne(type => MathTest)
    @JoinColumn({ name: 'mathtest_id', referencedColumnName: 'id' })
    mathTest: MathTest;
}