import { Module } from '@nestjs/common';
import { MathtesttaskController } from './mathtesttask.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MathTestTask } from './mathtesttask.entity';
import { MathtesttaskService } from './mathtesttask.service';

@Module({
    imports: [TypeOrmModule.forFeature([MathTestTask])],
    providers: [MathtesttaskService],
    controllers: [MathtesttaskController]
})
export class MathtesttaskModule {}
