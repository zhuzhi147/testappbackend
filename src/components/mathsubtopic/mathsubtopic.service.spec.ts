import { Test, TestingModule } from '@nestjs/testing';
import { MathsubtopicService } from './mathsubtopic.service';

describe('MathsubtopicService', () => {
  let service: MathsubtopicService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MathsubtopicService],
    }).compile();

    service = module.get<MathsubtopicService>(MathsubtopicService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
