import { Module } from '@nestjs/common';
import { MathsubtopicService } from './mathsubtopic.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MathSubtopic } from './mathsubtopic.entity';
import { MathsubtopicController } from './mathsubtopic.controller';

@Module({
  imports: [TypeOrmModule.forFeature([MathSubtopic])],
  providers: [MathsubtopicService],
  controllers: [MathsubtopicController]
})
export class MathsubtopicModule {}
