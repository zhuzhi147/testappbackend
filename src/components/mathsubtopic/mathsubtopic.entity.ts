import { PrimaryGeneratedColumn, Column, Entity, OneToMany, ManyToOne, JoinColumn } from "typeorm";
import { MathTopic } from "../math-topics/mathtopics.entity";
import { MathTest } from "../mathtest/mathtest.entity";

@Entity()
export class MathSubtopic {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 255})
    name: string; 
    
    @Column()
    mathtopic_id: number; 

    @ManyToOne(type => MathTopic)
    @JoinColumn({ name: 'mathtopic_id', referencedColumnName: 'id' })
    mathTopic: MathTopic;

    @OneToMany(type => MathTest, test => test.mathSubTopic, { cascade: ['insert', 'update'] })
    mathTests: MathTest[];
}