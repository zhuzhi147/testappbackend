import { Test, TestingModule } from '@nestjs/testing';
import { MathsubtopicController } from './mathsubtopic.controller';

describe('Mathsubtopic Controller', () => {
  let controller: MathsubtopicController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MathsubtopicController],
    }).compile();

    controller = module.get<MathsubtopicController>(MathsubtopicController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
