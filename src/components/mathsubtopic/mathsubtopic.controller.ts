import { Controller, Get, Param, Query } from '@nestjs/common';
import { MathsubtopicService } from './mathsubtopic.service';

@Controller('mathsubtopic')
export class MathsubtopicController {

    constructor(private mathSubtopicService: MathsubtopicService) {}

    @Get()
    async getMathSubtopics(@Query('topicId') topicId: number) {
        return await this.mathSubtopicService.getSubtopicsByTopicId(topicId);
    }
}
