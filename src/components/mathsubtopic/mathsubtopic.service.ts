import { Injectable } from '@nestjs/common';
import { MathSubtopic } from './mathsubtopic.entity';
import { Repository, getRepository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class MathsubtopicService {
    
    constructor(@InjectRepository(MathSubtopic) private mathSubtopicRepository: Repository<MathSubtopic>) {}

    async getSubtopicsByTopicId(topicId: number) : Promise<MathSubtopic[]> {
        return await this.mathSubtopicRepository.find({
            mathtopic_id:topicId
        });
    }
}
