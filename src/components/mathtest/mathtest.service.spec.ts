import { Test, TestingModule } from '@nestjs/testing';
import { MathtestService } from './mathtest.service';

describe('MathtestService', () => {
  let service: MathtestService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MathtestService],
    }).compile();

    service = module.get<MathtestService>(MathtestService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
