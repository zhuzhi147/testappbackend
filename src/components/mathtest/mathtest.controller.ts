import { Controller, Post, Body, UseInterceptors, UploadedFile, UploadedFiles, Query, Get } from '@nestjs/common';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer'
import { MathTest } from './mathtest.entity';
import { MathtestService } from './mathtest.service';
import { extname } from 'path'

@Controller('mathtest')
export class MathtestController {

    constructor(private readonly mathTestService: MathtestService) {}

    @Post()
    async create(@Body() mathTest: MathTest, @UploadedFiles() files) {
        return await this.mathTestService.create(mathTest);
    }

    @Get()
    async getTests(@Query('subtopicId') subtopicId: string) {
      return await this.mathTestService.getTestsByClass(subtopicId);
    }
}
