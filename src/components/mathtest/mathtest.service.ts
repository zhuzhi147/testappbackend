import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, createQueryBuilder, getRepository } from 'typeorm';
import { MathTest } from './mathtest.entity';
import { MathTopic } from '../math-topics/mathtopics.entity';
import { MathSubtopic } from '../mathsubtopic/mathsubtopic.entity';

@Injectable()
export class MathtestService {

    constructor(@InjectRepository(MathTest) private readonly mathTestRepository: Repository<MathTest>) {}

    public async create(mathTest: MathTest): Promise<MathTest> {
        return await this.mathTestRepository.save(mathTest);
    }

    public async getTestsByClass(subtopicId: string): Promise<MathTest[]> {
        const tests = await this.mathTestRepository
                .createQueryBuilder('mathTests')
                .from(MathSubtopic, 'math_subtopic')
                .leftJoin('math_test', 'test', 'math_subtopic.id = test.mathsubtopic_id')
                .where('math_subtopic.id = :id', { id: subtopicId })
                .select('DISTINCT test.name as name, test.id as id');
        return tests.getRawMany();
    }
}
