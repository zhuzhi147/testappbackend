import { Test, TestingModule } from '@nestjs/testing';
import { MathtestController } from './mathtest.controller';

describe('Mathtest Controller', () => {
  let controller: MathtestController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MathtestController],
    }).compile();

    controller = module.get<MathtestController>(MathtestController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
