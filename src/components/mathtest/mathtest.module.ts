import { Module } from '@nestjs/common';
import { MathtestService } from './mathtest.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MathTest } from './mathtest.entity';
import { MathtestController } from './mathtest.controller';

@Module({
  imports: [TypeOrmModule.forFeature([MathTest])],
  providers: [MathtestService],
  controllers: [MathtestController]
})
export class MathtestModule {}
