import { PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToMany, Entity } from "typeorm";
import { MathSubtopic } from "../mathsubtopic/mathsubtopic.entity";
import { MathTestTask } from "../mathtesttask/mathtesttask.entity";

@Entity()
export class MathTest {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 255 })
    name: string;

    @ManyToOne(type => MathSubtopic)
    @JoinColumn({ name: 'mathsubtopic_id', referencedColumnName: 'id' })
    mathSubTopic: MathSubtopic;

    @OneToMany(type => MathTestTask, test => test.mathTest, { cascade: ['insert', 'update'] })
    mathTasks: MathTestTask[];
}