import * as crypto from 'crypto';

export function passwordHash(password: string) {
    return crypto.createHmac('sha256', password)
        .digest('hex');
}