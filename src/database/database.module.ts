import { TypeOrmModule } from '@nestjs/typeorm';
import { databaseConnection } from './database.connection';
import { Module } from '@nestjs/common';

@Module({
    imports: [
        ...databaseConnection
    ],
    exports: [
        TypeOrmModule
    ]
})
export class DatabaseModule {}