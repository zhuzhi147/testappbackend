import { TypeOrmModule } from "@nestjs/typeorm";

  export const databaseConnection =  [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'password',
      database: 'testsystemapp_db',
      entities: [__dirname + '/../**/*.entity{.ts, .js}'],
      synchronize: true,
    }),
];
